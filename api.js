function consultSimpson(num) {
    fetch(`https://thesimpsonsquoteapi.glitch.me/quotes?count=${num}`)
        .then(function (response) {
            response.json()
                .then(function (simpsons) {
                    simpsons.map((simpson, index) =>
                        createSimpson(simpson, index)
                    )
                
                })
        })
}

function createSimpson(simpson, num) {
    let list = document.getElementById("listSimpson")
    let item = list.querySelector(`#simpson-${num}`)
    let img = item.getElementsByTagName("img")[0]
    img.setAttribute("src", simpson.image)
    let name = item.getElementsByTagName("p")[0]
    name.textContent = simpson.character

}

consultSimpson(2)

